#Frith canvas quiz checker progress

[https://gitlab.com/alnVT/frithkeylogger](https://www.google.com/url?q=https://gitlab.com/alnVT/frithkeylogger&sa=D&ust=1555108027248000).

### Current tutorial: [https://www.instructables.com/id/Simple-Keylogger-Python/](https://www.instructables.com/id/Simple-Keylogger-Python/&sa=D&ust=1555108027248000)

Started 3/19/19, finished 4/11/19. This serves as history and
recreation documentation for a full sign in system in the Frith Lab.
Earliest is at the bottom, each entry is ordered from top to bottom but
the entries themselves are bottom-\>top. More description can/should be
added.

##Next steps:
-----------

### Keylogger: {#h.9zo1q0kmftf .c5 .c12}

1.  Fix keylogger input recording

1.  Combination to end process
2.  Special character support
3.  Try to restrict it to just the webpage
4.  Clear records regularly

2.  Script to check against csv

###  {#h.6yj3hphlhagz .c5 .c12 .c10}

### Server: {#h.itioz0pfu5pf .c5 .c12}

3.  Fix apr-util compilation

1.  Build web interface for website

#Entries: 
========

##4/11/19
-------

sed 's/ /\\\\\\n/7;P;D' file.txt

##3/25/19: 1:20-3:30pm
--------------------

Started building a script to parse/compare with the csv. Made a plan for
finishing the data storage part of the project.

The script stopped throwing the first error but now can freeze the
computer. Tried to add a keyboard combination to exit() the script but
it doesn't work as of yet. Look at the error when in the lab next.

MacOS won't work as of now - no admin/sudo access.

### Plan: {#h.bonbitwxtd9f .c5 .c12}

Students disallowed from the lab are stored, and removed by ULA when
completed. Determine a format for the data storage, either Rod can send
me a relevant file or I'll make a processing script (Rod will send me
the output).

ULAs verify grade completion and mark it SOMEWHERE.

##3/24/19: 3:30-8:00pm
--------------------

Configured git repositories to be able to develop at home once this
environment is setup, and for easy deployment. Use powershell/command
prompt to run the script in python, use cygwin for updating with git.

Pyhook was installed SUCCESSFULLY by running the raw pip command on the
downloaded file in Downloads.
\\Users\\Student\\AppData\\Local\\Programs\\Python\\Python37\\Scripts\\pip.exe
install pyHook-1.5.1-cp37m-win\_amd64.whl.

The script runs, but fails due to some encoding issue as soon as typing
begins. Print a debug of a swipe to check at home (done). Will continue
to work on that at home.

Used this [reference from
VaporCreations](https://www.google.com/url?q=https://stackoverflow.com/questions/9436860/apache-httpd-setup-and-installation&sa=D&ust=1555108027251000) to
install apr, apr-util, apache. Working so far, finished apr (7:21pm).
Installing expat packages from the cygwin manager fixed the expat.h
compilation issue. GCC is leaving the compilation now (make, for apr.
Configuration complete) due to "relocation truncated to fit:
R\_X86\_64\_PC32" and the linker is freaking out. Fix this. Make install
failed. Trying configuring httpd regardless of this being functional
(7:34p). Apr-util can't be found (7:40).

##3/21/19: 6:30-9:20
------------------

Spent most of the time on my mac. pyHook is the limitation on the signin
computer and doesn't seem to be an easy fix. I began building the
analysis python script; the equivalent on macOS requires Cocoa/Quartz
integration to the system level, partially due to BSD but mostly system
integrity protection. Brew messes up python, fixed that - solution notes
on desktop.

Analyzed hardcopy of existing website code.

Started learning python. Get a reference book. Currently I'm using:

1.  [https://realpython.com/python-csv/](https://www.google.com/url?q=https://realpython.com/python-csv/&sa=D&ust=1555108027252000)
2.  [https://www.w3schools.com/python/python\_examples.asp](https://www.google.com/url?q=https://www.w3schools.com/python/python_examples.asp&sa=D&ust=1555108027253000)

##3/21/19: 3-4:45
---------------

Got python working natively in windows, pip is working. Keylogger has
failed to work so far. Have tried a second tutorial, pyHook is the issue
currently.

##3/20/19: 3:20-3:50
------------------

Focused on fixing apache. Fiddled a bit, tried some things, nothing
worked. Tried to determine whether cygwin uses a windows source for APR
or a unix source. Found a site with a full tutorial (cygwin on windows
running a lighthttpd server) - it filled the ram and lagged the computer
for about 10mins. Also tried to figure out where to put the source files
to compile with GCC - moved to /home/Student/Downloads/apr-name/.

##3/19/19: From 4ish to 7ish minus a class in between
---------------------------------------------------

Downloaded and installed:

1.  7zip
2.  Cygwin
3.  [Apache](https://www.google.com/url?q=http://httpd.apache.org/download.cgi%23apache24&sa=D&ust=1555108027253000)

Linux subsystem for Windows won't work because the computer is on
Windows 7. Cygwin was downloaded to support python but appears to
support an apache server as well. This has been giving me issues,
currently requires APR library. Python appears to work.

### Current research articles:

-   [https://httpd.apache.org/docs/1.3/cygwin.html](https://www.google.com/url?q=https://httpd.apache.org/docs/1.3/cygwin.html&sa=D&ust=1555108027254000)
-   [https://stackoverflow.com/questions/9436860/apache-httpd-setup-and-installation](https://www.google.com/url?q=https://stackoverflow.com/questions/9436860/apache-httpd-setup-and-installation&sa=D&ust=1555108027254000)

(in reverse chronologic order)

-----------------BEGIN TRANSCRIPT------------------------
=========================================================

Reference resources (for all time):
-----------------------------------

-   [http://httpd.apache.org/docs/2.4/platform/windows.html](https://www.google.com/url?q=http://httpd.apache.org/docs/2.4/platform/windows.html&sa=D&ust=1555108027255000)
-   [https://stackoverflow.com/questions/9705982/pythonw-exe-or-python-exe/30313091\#30313091](https://www.google.com/url?q=https://stackoverflow.com/questions/9705982/pythonw-exe-or-python-exe/30313091%2330313091&sa=D&ust=1555108027255000)
-   [http://www.covingtoninnovations.com/mc/winforunix.html](https://www.google.com/url?q=http://www.covingtoninnovations.com/mc/winforunix.html&sa=D&ust=1555108027255000)
-   [https://www.lfd.uci.edu/\~gohlke/pythonlibs/](https://www.google.com/url?q=https://www.lfd.uci.edu/~gohlke/pythonlibs/&sa=D&ust=1555108027256000)

Other notes:
------------
