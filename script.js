//Andrew Nyland, updated 4/11/19
var keys = "";
var students = "";
var recording = false;

onload = function () {
    //Read key inputs
    document.addEventListener("keydown", function (e) {
    var key = e.key | e.which;
    var charr = String.fromCharCode(key);
    //console.log("\"" + charr + "\" pressed", recording, keys.length);
    if (recording) {
        keys += charr;
    }
    if (keys.length > 8) {
        subm();
    }
    if (/^\u00BA/.test(charr)) {
        recording = true;
    }
});

//Event handler for checking input
document.getElementById("student_id").addEventListener("blur", subm);
}

function subm() {
    if (!keys) {return false;}
    console.log("Swiped: "+keys, students.includes(keys));
    if (students.includes(keys)) {
       setTimeout(function() {
           alert("Student has not completed quiz.");
       }, 100);
    }
    keys = ""; //reset
    recording = false;
}
