#!/usr/bin/env bash
cp students.txt update/students_source.txt
cd update
words=`wc students_source.txt | awk '{print $2}'`
sed 's/ /\\\n/7;P;D' students_source.txt > students.txt
cat first.js students.txt second.js > ../new_script.js
echo "$words students processed."

#ADD WINDOWS copy to clipboard
